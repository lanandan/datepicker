package com.example.datepicker;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;


public class MainActivity extends Activity {

	
	ImageView bday;
	DatePicker datePicker;
	EditText bdayEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        datePicker = (DatePicker)findViewById(R.id.date_picker);
		bday = (ImageView) findViewById(R.id.pro_bday_calander_icon);
		bdayEdit = (EditText) findViewById(R.id.pro_bday_editview);
		
		 bday.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					VisaliDatePicker.datePicker(MainActivity.this, bdayEdit);
					//showDialog(DATE_DIALOG_ID);
					
				}
			});
    }

}
