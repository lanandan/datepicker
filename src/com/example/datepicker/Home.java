package com.example.datepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home extends Activity{
	
	Button btn_next;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkbox_custom);
        btn_next=(Button)findViewById(R.id.btn_submit);
        
        btn_next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent Railway=new Intent(Home.this,MainActivity.class);
				startActivity(Railway);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			}
		});
	}

}
