package com.example.datepicker;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class VisaliDatePicker {
	public static Context datePicker;
	public static int getDate;
	public static int getMonth;
	public static int getYear;
	public static int currentyear;
	public static int currentdate;
	public static int currentmonth;
	public static boolean isLeapyear = false;
	public static NumberPicker date;
	public static NumberPicker month;
	public static NumberPicker year;
	public static Button done;
	public static RelativeLayout dismiss;

	public static Dialog dialog;

	public static void datePicker(final Context loadingContext,
			final EditText tvdate) {
		dialog = new Dialog(loadingContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.setContentView(R.layout.datepicker);
		dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		
		
		dialog.getWindow().setBackgroundDrawableResource(R.drawable.date_bg);
		//dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		Calendar calendar = Calendar.getInstance();
		currentyear = calendar.get(Calendar.YEAR);
		currentdate = calendar.get(Calendar.DATE);
		currentmonth = calendar.get(Calendar.MONTH) + 1;

		date = (NumberPicker) dialog.findViewById(R.id.date);
		month = (NumberPicker) dialog.findViewById(R.id.month);
		year = (NumberPicker) dialog.findViewById(R.id.year);
		done = (Button) dialog.findViewById(R.id.btnSave);
		dismiss = (RelativeLayout) dialog.findViewById(R.id.dismiss);

		final String[] values = new String[12];
		values[0] = "Jan";
		values[1] = "Feb";
		values[2] = "Mar";
		values[3] = "Apr";
		values[4] = "May";
		values[5] = "Jun";
		values[6] = "Jul";
		values[7] = "Aug";
		values[8] = "Sep";
		values[9] = "Oct";
		values[10] = "Nov";
		values[11] = "Dec";
		month.setMaxValue(values.length - 1);
		month.setMinValue(0);
		month.setDisplayedValues(values);

		//date.setMaxValue(31);
		
		if (currentmonth == 1 || currentmonth == 3 || currentmonth == 5 || currentmonth == 7
				|| currentmonth == 8 || currentmonth == 10 || currentmonth == 12) {
			date.setMaxValue(31);
		} else if (currentyear % 4 == 0 && month.getValue() == 2) {
			date.setMaxValue(29);
		} else if (month.getValue() == 2) {
			date.setMaxValue(28);
		} else {
			date.setMaxValue(30);
		}
		date.setValue(currentdate);
		date.setMinValue(1);

		//month.setMaxValue(currentmonth);
		
		
		month.setValue(currentmonth);
		month.setMinValue(1);

		year.setMaxValue(currentyear+50);
		year.setValue(currentyear);
		year.setMinValue(1975);

		year.setWrapSelectorWheel(true);
		month.setWrapSelectorWheel(true);
		date.setWrapSelectorWheel(true);

		date.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		month.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		year.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

		done.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				
				
				getDate = date.getValue();
				getYear = year.getValue();
				getMonth = month.getValue();
				if(isFutureDate(getYear, getMonth, getDate)){
					Toast.makeText(loadingContext, "Please select the valid date", 6000).show();
				}else{
					tvdate.setText(Integer.valueOf(getDate) + "/"+Integer.valueOf(getMonth) + "/"
							+ Integer.valueOf(getYear));
					dialog.dismiss();
				}
				
			}
		});

		year.setOnValueChangedListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal) {
				
				if (month.getValue() == 1 || month.getValue() == 3 || month.getValue() == 5 || month.getValue() == 7
						|| month.getValue() == 8 || month.getValue() == 10 || month.getValue() == 12) {
					date.setMaxValue(31);
				} else if (year.getValue() % 4 == 0 && month.getValue() == 2) {
					date.setMaxValue(29);
				} else if (month.getValue() == 2) {
					date.setMaxValue(28);
				} else {
					date.setMaxValue(30);
				}

				//checkDate(year.getValue(), month.getValue(), date.getValue());
			}
		});

		month.setOnValueChangedListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal) {
				checkMonth(year.getValue(), month.getValue(), date.getValue());

			}
		});

		date.setOnValueChangedListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal) {
				//checkDate(year.getValue(), month.getValue(), date.getValue());

			}
		});

		dismiss.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
	}

	/*
	 * public static void dateChange() { if (year.getValue() == currentyear) {
	 * month.setMaxValue(currentmonth); month.setWrapSelectorWheel(false);
	 * 
	 * date.setWrapSelectorWheel(false); } if (year.getValue() == currentyear &&
	 * month.getValue() == currentmonth) { // year.setMaxValue(currentyear);
	 * date.setMaxValue(currentdate); // month.setMaxValue(currentmonth); //
	 * year.setWrapSelectorWheel(false); month.setWrapSelectorWheel(false);
	 * 
	 * date.setWrapSelectorWheel(false);
	 * 
	 * } }
	 */

	public static void checkDate(int curyear, int curmonth, int curdate) {
		if (curmonth == 1 || curmonth == 3 || curmonth == 5 || curmonth == 7
				|| curmonth == 8 || curmonth == 10 || curmonth == 12) {
			date.setMaxValue(31);
		} else if (curyear % 4 == 0 && month.getValue() == 2) {
			date.setMaxValue(29);
		} else if (month.getValue() == 2) {
			date.setMaxValue(28);
		} else {
			date.setMaxValue(30);
		}

		if (curyear == currentyear) {
			month.setMaxValue(currentmonth);
			// date.setMaxValue(currentdate);
		} else {
			month.setMaxValue(12);
		}
		if (curyear == currentyear && curmonth == currentmonth) {
			date.setMaxValue(currentdate);
		}
		month.setWrapSelectorWheel(false);
		year.setWrapSelectorWheel(false);
		date.setWrapSelectorWheel(false);
	}
	
	public static void checkMonth(int curyear, int curmonth, int curdate) {
		if (curmonth == 1 || curmonth == 3 || curmonth == 5 || curmonth == 7
				|| curmonth == 8 || curmonth == 10 || curmonth == 12) {
			date.setMaxValue(31);
		} else if (curyear % 4 == 0 && month.getValue() == 2) {
			date.setMaxValue(29);
		} else if (month.getValue() == 2) {
			date.setMaxValue(28);
		} else {
			date.setMaxValue(30);
		}
	}
	
	public static boolean isFutureDate(int curyear, int curmonth, int curdate){
		if(curyear==currentyear){
			if(curmonth==currentmonth){
				if(curdate>currentdate){
					return true;
				}else{
					return false;
				}
			}else if(curmonth>currentmonth){
				return true;
			}
		}else if(curyear>currentyear){
			return true;
		}
		return false;
	}
}

